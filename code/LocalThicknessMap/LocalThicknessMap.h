#pragma once

#include "Cube.h"
#include "DistanceMap.h"

#define LOCAL_DISTANCE_EPSILON 0.5f

//Search for the biggest sphere that contains the "pos" voxel.
//Input: -pos: coordinate of one voxel
//       -dm: distance map
//		 -searchRange: The radius to search
//Output: diameter of the biggest sphere
static float LocalDistanceMapBruteForceSearch(Int3 pos, const Cube<float>& dm, int searchRange) {
	float bestVal = dm(pos);  
	for (int z =std::max(pos.z-searchRange,0); z < std::min(pos.z+searchRange,dm.DimZ()); z++) {
		for (int y = std::max(pos.y - searchRange, 0); y < std::min(pos.y + searchRange, dm.DimY()); y++) {
			for (int x = std::max(pos.x - searchRange, 0); x < std::min(pos.x + searchRange, dm.DimX()); x++) {				
                Int3 pos2(x, y, z);
				if (dm(pos2) > 0) {
					//Squared distance between pos and pos2
					float distanceSquared = DistanceSquared(pos, pos2);
					float tmpVal = dm(pos2);
					//Check if the distance between pos and pos2 is smaller than the distance map at pos2 
					//and if the distance map at pos2 is the largest value
					if ((distanceSquared <= Square(tmpVal + LOCAL_DISTANCE_EPSILON)) && (tmpVal > bestVal)) {
						bestVal = tmpVal;
					}
				}                    
			}
		}
	}  
	return bestVal * 2.0f; //Calculate the diameter
}

//Calculate the local thickness map from a distance map
//Input: -dm:  distance map
//Output: local thickness map
static Cube<float> LocalThicknessMapBruteForce(const Cube<float>& dm) {
	std::cout << "\nCalculating Local Thickness Map: " << std::endl;
	//Compute max of distanceMap to restrict search
	float maxVal = 0;
	for (int z = 0; z < dm.DimZ(); z++)
		for (int y = 0; y < dm.DimY(); y++)
			for (int x = 0; x < dm.DimX(); x++)
				maxVal = std::max(maxVal,dm(x, y, z));
	int searchRange = int(maxVal + 2.0);

	Cube<float> ltm(dm.DimX(), dm.DimY(), dm.DimZ());
	int counter = 0;
	// Iterate each voxel
    #pragma omp parallel for schedule(dynamic, 1)
	for (int z = 0; z < dm.DimZ(); z++) {
		// Progress Log
		#pragma omp critical
		std::cout << float(++counter) / dm.DimZ() * 100 << "%" << "            \r";

		for (int y = 0; y < dm.DimY(); y++) {
			for (int x = 0; x < dm.DimX(); x++) {
				Int3 pos(x, y, z);
				if (dm(pos) > 0.0f) {
					//Search for the biggest sphere that contains pos
					ltm(pos) = LocalDistanceMapBruteForceSearch(pos, dm, searchRange);
				}
				else {
					ltm(pos) = 0.0f; //pos is outside the mask
				}
			}
		}
	}
	return ltm;
}
