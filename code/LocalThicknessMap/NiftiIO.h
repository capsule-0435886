#pragma once

#include "nifti1.h"
#include "Cube.h"
#include <cstring>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <algorithm>
#include <stdio.h>
#include <stdexcept>

typedef long long int int64;

class NiftiIO
{
public:
	//Read a file into cube
	//Input:  - fileName: the path to the input file
	//Output  - cube: containing the file 
	template <typename T>
	static void Read(Cube<T>& cube, const std::string& fileName) {
		FILE* file;
		file = fopen(fileName.c_str(), "rb");

		nifti_1_header header;
		//Read the header of the input file
		if (fread(&header, sizeof(struct nifti_1_header), 1, file) != 1)
		{
			throw std::runtime_error("Could not read header.");
		}
		//Reposition the stream posistion indicator to the body of input file
		fseek(file, long(header.vox_offset), SEEK_SET);

		int64 byteCount = cube.DimX()*cube.DimY()*cube.DimZ() * sizeof(T);
		int64 blockSize = 1024 * 1024 * 256;
		unsigned char* p = (unsigned char*)cube.Pointer();
		//Read every block
		for (int64 start = 0; start < byteCount; start += blockSize) {
			int64 n = std::min(blockSize, byteCount - start);
			int64 readCount = fread(p + start, n, 1, file);
			if (readCount != 1)
				throw std::runtime_error("Only some data read.");
		}
		fclose(file);
	}

	//Save a cube to file
	//Input:  - cube: the object Cube 
	//Output: - fileName: the path to the saved file 
	static void Save(const Cube<float>& cube, const std::string& fileName) {
		FILE* file;
		file = fopen(fileName.c_str(), "wb");

		nifti_1_header header;
		memset(&header, 0, sizeof(struct nifti_1_header));

		header.sizeof_hdr = sizeof(struct nifti_1_header);
		header.vox_offset = sizeof(struct nifti_1_header) + 4;

		header.datatype = 16;

		header.dim[0] = 5;
		header.dim[1] = cube.DimX();
		header.dim[2] = cube.DimY();
		header.dim[3] = cube.DimZ();
		header.dim[4] = 1;
		header.dim[5] = 1;

		header.pixdim[0] = 1.0f;
		header.pixdim[1] = 1.0f;
		header.pixdim[2] = 1.0f;
		header.pixdim[3] = 1.0f;

		header.scl_slope = 1.0f;

		header.magic[0] = 'n';
		header.magic[1] = '+';
		header.magic[2] = '1';
		header.magic[3] = '\0';
		//Write the header of the file
		fwrite(&header, sizeof(struct nifti_1_header), 1, file);
		int dummy = 0;
		//Write a dummy to the file
		fwrite(&dummy, 4, 1, file);

		int64 byteCount = cube.DimX()*cube.DimY()*cube.DimZ() * sizeof(float);
		int64 blockSize = 1024 * 1024 * 256;
		const unsigned char* p = (const unsigned char*)cube.Pointer();
		//Write every block
		for (int64 start = 0; start < byteCount; start += blockSize) {
			int64 n = std::min(blockSize, byteCount - start);
			int64 writeCount = fwrite(p + start, n, 1, file);
			if (writeCount != 1)
				throw std::runtime_error("Only some data written.");
		}
		fclose(file);
	}
};