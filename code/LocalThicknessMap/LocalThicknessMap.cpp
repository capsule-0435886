
#include "NiftiIO.h"
#include "Cube.h"
#include "DistanceMap.h"
#include "LocalThicknessMap.h"
#include "Interpolator.h"
#include <stdio.h>
#include <string>

//Convert the 3D trabecular image into a binary mask
//Input: - cube: 3d trabecular image
//       - threshold: threshold to convert the 3D image to a binary mask
//       - aboveThreshold = false, mask value of voxels of which intensities are smaller than threshold is 0
//								   mask value of voxels of which intensities are bigger than threshold is 1
//       - aboveThreshold = true, mask value of voxels of which intensities are smaller than threshold is 1
//								  mask value of voxels of which intensities are bigger than threshold is 0
static Cube<char> CreateMask(const Cube<short>& cube, float threshold, bool aboveThreshold = false) {
	Cube<char> mask(cube.DimX(), cube.DimY(), cube.DimY());
#pragma omp parallel for
	for (int z = 0; z < cube.DimZ(); z++) {
		for (int y = 0; y < cube.DimY(); y++) {
			for (int x = 0; x < cube.DimX(); x++) {
				if (aboveThreshold) {
					if (cube(x, y, z) >= threshold)
						mask(x, y, z) = 0;//inside bone
					else
						mask(x, y, z) = 1;//outside	bone (none-bone)
				}
				else {
					if (cube(x, y, z) < threshold)
						mask(x, y, z) = 0;//outside	bone (none-bone)
					else
						mask(x, y, z) = 1;//inside bone
				}
			}
		}
	}
	return mask;
}

//Calculate the average thickness
//Input: - ltm: local thickness map
//       - mask: binary mask
static double Average(const Cube<float>& ltm, const Cube<char>& mask, double voxelsize) {
	double sums = 0.0;
	int counts = 0;

	for (int z = 0; z < ltm.DimZ(); z++) {
		double sum = 0;
		int count = 0;
		for (int y = 0; y < ltm.DimY(); y++) {
			for (int x = 0; x < ltm.DimX(); x++) {
				if (mask(x, y, z) == 1) {
					count++;
					sum += ltm(x, y, z);
				}
			}
		}
		sums += sum;
		counts += count;
	}
	double mean = sums / counts * voxelsize; // Scale to voxelsize

	return mean;
}

int main()
{
	//scale factor for upsampling. 
	//factor = 1: No upsampling
	//factor = 2: Upsampling by 2
	float factor = 1;
    std::cout << "It requires some time to calculate " << std::endl;

	std::string dataPath = "../../data/";
	std::string fileName= "trabecular.nii";
	std::string sInput = dataPath + fileName;;
	double voxelsize = 20; //20 micrometer
	Cube<short> cube(100, 100, 100); //100 voxel x 100 voxel x 100 voxel
	
	NiftiIO::Read(cube, sInput);
	//If factor is not 1, up-sample the cube with the factor. The dimensions are enlarged with the factor.
	if (factor != 1)
    {
		Cube<short> upSCube(int(cube.DimX() * factor), int(cube.DimY() * factor), int(cube.DimZ() * factor));
		ResampleTrilinear(upSCube, cube, 1 / factor);
		cube = upSCube;
	}

	voxelsize = voxelsize / factor;
	float threshold = 2000.0f;
	//Create a binary mask to calculate mean thickness when aboveThreshold = false, or mean spacing when aboveThreshold = true
	Cube<char> mask = CreateMask(cube, threshold, false);

    //Calculate the distance map
	Cube<float> dm = DistanceMapBruteForce(mask);
	sInput = "DMThicknessBruteforce.nii";
	//Save the distance map
	NiftiIO::Save(dm, sInput);

    //Calculate the local thickness map
	Cube<float> ltm = LocalThicknessMapBruteForce(dm);
	//Calculate the mean thickness/spacing
	double mean = Average(ltm, mask, voxelsize);

	sInput = "LTMThicknessBruteforce.nii";
	//Save the local thickness map
	NiftiIO::Save(ltm, sInput);

	std::cout << "Mean thickness/spacing: " << mean << std::endl;
	system ("pause");
}