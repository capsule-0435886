#pragma once

#include "Cube.h"
#include <cmath>
#include <limits>
#include <iostream>

//Square of a float number
static float Square(float x) { return x * x; }

//Calculate the Euclidean distance between two voxels
//Input: a, b: coordinate of two voxels 
//Output: Euclidean distance between two voxels
static float Distance(Int3 a, Int3 b) {
	return std::sqrt(Square(float(a.x - b.x)) + Square(float(a.y - b.y)) + Square(float(a.z - b.z)));
}

//Calculate square of the Euclidean distance between two voxels
//Input: a, b: coordinate of two voxels	
//Output: square of Euclidean distance between two voxels
static float DistanceSquared(Int3 a, Int3 b) {
	return Square(float(a.x - b.x)) + Square(float(a.y - b.y)) + Square(float(a.z - b.z));
}

//Calculate the distance map from a binary mask
//Input: mask: binary mask
//Output: distance map
static Cube<float> DistanceMapBruteForce(const Cube<char>& mask) {
	std::cout << "Calculating Distance Map: " << std::endl;
	Cube<float> distanceMap(mask.DimX(), mask.DimY(), mask.DimZ());
	int counter = 0;

	// Iterate each voxel
	#pragma omp parallel for schedule(dynamic, 1)
	for (int z = 0; z < distanceMap.DimZ(); z++) {
		// Progress Log
		#pragma omp critical
		std::cout << float(++counter) / mask.DimZ() * 100 << "%" << "\r";

		for (int y = 0; y < distanceMap.DimY(); y++) {
			//Start with a large search radius to capture the full image
			int searchRadius = mask.DimX() + mask.DimY() + mask.DimZ();

			for (int x = 0; x < distanceMap.DimX(); x++) {				
				Int3 center(x, y, z);
				// Skip mask voxels
				if (!mask(x, y, z)) {
					distanceMap(x, y, z) = 0;
					continue;
				}

				// Find minimum distance to a mask voxel
				float minDistance = std::numeric_limits<float>::max();
				//Search over a certain radius 
				for (int zz = std::max(z-searchRadius,0); zz < std::min(z+searchRadius,distanceMap.DimZ()); zz++) {
					for (int yy = std::max(y - searchRadius, 0); yy < std::min(y + searchRadius, distanceMap.DimY()); yy++) {
						for (int xx = std::max(x - searchRadius, 0); xx < std::min(x + searchRadius, distanceMap.DimX()); xx++) {
							//Skip voxels inside the mask
							if (mask(xx, yy, zz))
								continue;
							float distance = Distance(center, Int3(xx, yy, zz));
							//Check if the distance between center and the voxel at (xx, yy, zz) is the smallest value
							if (distance <= minDistance) {
								minDistance = distance;
							}
						}
					}
				}
				//The smallest distance is saved into the distance map
				distanceMap(x, y, z) = minDistance;
				searchRadius = int(minDistance + 3.0f);//The next guy cannot be far from the current
			}
		}
	}
	return distanceMap;
}

