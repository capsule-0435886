#pragma once

#include <vector>

class Int3 {
public:
	int x;//The first element
	int y;//The second element
	int z;//The third element

	//Default constructor
	Int3() : x(0), y(0), z(0) {}

	//Constructor with an element for initialization
	explicit Int3(int init) :x(init), y(init), z(init) {}

	//Constructor that takes the elements as input.
	Int3(int x, int y, int z) : x(x), y(y), z(z) {}
};

class Double3 {
public:
	double x;//The first element
	double y;//The second element
	double z;//The third element

	//Default constructor
	Double3() {}

	//Constructor with an element for initialization
	explicit Double3(double init) :x(init), y(init), z(init) {}

	//Constructor that takes the elements as input.
	Double3(double x, double y, double z) : x(x), y(y), z(z) {}

};

//Contains the data and the structure
template<typename T>
class Cube {
public:
	std::vector<T> data;//Contains the data
	int dimX;//X Dimension
	int dimY;//Y Dimension
	int dimZ;//Z Dimension
public:
	//Default constructor
	Cube() {}

	//Constructor that allocates memory for a given size
	Cube(int dimX, int dimY, int dimZ) : data(dimX*dimY*dimZ), dimX(dimX), dimY(dimY), dimZ(dimZ) {}

	//Returns an element (by reference)
	T& operator()(int x, int y, int z) { return data[x+y*dimX+z*dimX*dimY]; }
	T& operator()(Int3 pos) { return data[pos.x + pos.y * dimX + pos.z * dimX*dimY]; }
	const T& operator()(int x, int y, int z) const { return data[x + y * dimX + z * dimX*dimY]; }
	const T& operator()(Int3 pos) const { return data[pos.x + pos.y * dimX + pos.z * dimX*dimY]; }

	//Returns the X dimension
	int DimX()const { return dimX; }

	//Returns the Y dimension
	int DimY()const { return dimY; }

	//Returns the Z dimension
	int DimZ()const { return dimZ; }

	//Returns the pointer to the data.
	T* Pointer() { return &data[0]; }
	const T* Pointer() const { return &data[0]; }
};