#pragma once

#include <cmath>
#include "Cube.h"

//Contain an object of Cube cube, extending the cube by assigning a new element the closest element inside the cube 
template<typename T>
class BorderExtenderCube {
	const Cube<T>& cube;
public:
	BorderExtenderCube(const Cube<T>& cube) :cube(cube) {}
	//Return the X dimension of cube
	int DimX() const { return cube.DimX(); }
	//Return the Y dimension of cube
	int DimY() const { return cube.DimY(); }
	//Return the Z dimension of cube
	int DimZ() const { return cube.DimZ(); }
	//Assign the data of (x,y,z) the data of the closest element inside cube 
	T operator() (int x, int y, int z) const {
		x = std::max(std::min(x, cube.DimX() - 1), 0);
		y = std::max(std::min(y, cube.DimY() - 1), 0);
		z = std::max(std::min(z, cube.DimZ() - 1), 0);
		T result = cube(x, y, z);
		return result;
	}
};

//Contain an object of BorderExtenderCube cube, extending cube by intepolating 
template<typename T>
class InterpolatorTrilinear {
	const BorderExtenderCube<T>& cube;
public:
	InterpolatorTrilinear(const BorderExtenderCube<T>& cube) :cube(cube) {}
	//Return the X dimension of cube
	int DimX() const { return cube.DimX(); }
	//Return the Y dimension of cube
	int DimY() const { return cube.DimY(); }
	//Return the Z dimension of cube
	int DimZ() const { return cube.DimZ(); }
	//Return an element at (x,y,z) by interpolating 
	template<typename Real>
	T operator()(Real x, Real y, Real z) const {
		Real xd = x - std::floor(x);
		Real yd = y - std::floor(y);
		Real zd = z - std::floor(z);

		int downX = static_cast<int>(std::floor(x)); int upX = downX + 1;
		int downY = static_cast<int>(std::floor(y)); int upY = downY + 1;
		int downZ = static_cast<int>(std::floor(z)); int upZ = downZ + 1;
		Real tmp = Real(1) - zd;
		Real i1 = cube(downX, downY, downZ) * tmp + cube(downX, downY, upZ) * zd;
		Real i2 = cube(downX, upY, downZ) * tmp + cube(downX, upY, upZ) * zd;
		Real j1 = cube(upX, downY, downZ) * tmp + cube(upX, downY, upZ) * zd;
		Real j2 = cube(upX, upY, downZ) * tmp + cube(upX, upY, upZ) * zd;

		tmp = Real(1) - yd;
		Real w1 = i1 * tmp + i2 * yd;
		Real w2 = j1 * tmp + j2 * yd;

		tmp = Real(1) - xd;
		T result = T(w1 * tmp + w2 * xd);
		return result;
	}
};

//Resample the cube following the algorithm trilinear interpolation
//Input:  - src: the source cube 
//        - scalingFactor: scaling factor to calculate the data of dst 
//Output: - dst: the destination cube
template<typename T>
void ResampleTrilinear(Cube<T>& dst, const InterpolatorTrilinear<T>& src, double scalingFactor) {
	int dimX = dst.DimX();
	int dimY = dst.DimY();
	int dimZ = dst.DimZ();

#pragma omp parallel for
	for (int z = 0; z < dimZ; z++) {
		for (int y = 0; y < dimY; y++) {
			for (int x = 0; x < dimX; x++) {
				double xD = static_cast<double>(x) * scalingFactor;
				double yD = static_cast<double>(y) * scalingFactor;
				double zD = static_cast<double>(z) * scalingFactor;
				dst(x, y, z) = src(xD, yD, zD);
			}
		}
	}
}

//Resample the cube following the algorithm trilinear interpolation
//Input:  - src: the source cube 
//        - scalingFactor: scaling factor to calculate the data of dst 
//Output: - dst: the destination cube
template<typename T>
void ResampleTrilinear(Cube<T>& dst, const Cube<T>& src, double scalingFactor) {
	BorderExtenderCube<T> bCube(src);
	InterpolatorTrilinear<T> texture(bCube);
	ResampleTrilinear(dst, texture, scalingFactor);
}