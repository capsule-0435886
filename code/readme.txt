#The brute force algorithm to calculate mean thickness/spacing

The code is implemented simply in C++ to measure trabecular thickness and spacing of bone images.

#Compile and run

##Compile and run in Code Ocean

###Structure in Code Ocean

The code is uploaded in the folder "code", while the data can be found in the folder "data". The results of code run are saved in the folder "results".

###Steps to compile and run the code to measure trabecular thickness of the input image in the folder "data":

1. Open the code in Code Ocean
2. Click on the button "Reproducible Run" on the right panel
3. Wait until the run finishes
The result of the mean thickness is shown in the console or in the result file "output". The result files "DMThicknessBruteforce.nii", "LTMThicknessBruteforce.nii" and "output" can be found in the folder “results”

The data and code cannot be replaced if the code runs directly in Code Ocean. This means, the user can neither run the code with other data nor measure trabecular spacing in Code Ocean.

##Compile and run on a local computer

###Prerequisite

CMmake Version 3.1 or newer is required to generate the project files, e.g. for Visual Studio 2019, which can then used to compile the code. Our code only uses C++ with the standard library and OpenMP statements.
The code may be compiled on any platform and C++ compiler but was only tested with Windows 7 and 10 with Visual Studio 2019.

###Steps to compile and run the code, in order to calculate the mean thickness of the given input image:

1.	Download the code and data from Code Ocean 
2.	Extract the code to a folder, named “code”, and the data to the folder, named “data”, into the same parent folder.
3.	Create a folder for the build, named “build” in the same parent folder
4.	Open a command line, run the command: 
cmake -S <path to the source code folder> -B <path to the build folder>
5.	To compile and build code, run the command:
cmake --build <path to the build folder> --config release
6.	On the command line, go the folder “build\release”, call LocalThicknessMap.exe. Wait until the run is finished.
7.	The result of the mean thickness is shown in the console. The result files are "DMThicknessBruteforce.nii" and "LTMThicknessBruteforce.nii" can be found in the folder “build\release”

In order to calculate the mean spacing of the given input image, open the file “code\LocalThicknessMap\LocalThicknessMap.cpp” with an editor, for example Notepad, and change “false” to “true” on the line 95. Then repeat the steps 4-7 above.

###Reuse - Run code with other data

####Thickness/spacing measurement of a dataset from the provided database in Figshare 

The user can download a database of 40 datasets from Figshare. (https://doi.org/10.6084/m9.figshare.9864677)
To calculate the mean thickness or mean spacing of an arbitrary dataset from the database, replace the file “trabecular.ini” in the data folder “data” with the file of the same name of this dataset. Then, repeat the steps 6-7 above. 

####Thickness/spacing measurement of images beyond the provided database

The values of the following parameters in the file “code\LocalThicknessMap\LocalThicknessMap.cpp” must be replaced if necessary:
1.	“fileName”: the name of the input image – line of code 78
2.	“voxelsize”: the voxel size with the unit µm – line of code 80
3.	The dimension of the parameter “cube”: the dimension of the input image with the unit “voxel” - line of code 81
4.	“threshold”: the threshold for generating a binary mask for the input image – line of code 93
In addition, the input image must be copied to the folder “data”. Then repeat the steps 4-7 above.
