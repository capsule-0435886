#!/usr/bin/env bash
clear
cd LocalThicknessMap

g++ -std=c++11 LocalThicknessMap.cpp -o localThicknessMap

./localThicknessMap

mv *.nii ../../results